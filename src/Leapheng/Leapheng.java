package Leapheng;

import com.jakewharton.fliptables.FlipTable;
import com.jakewharton.fliptables.FlipTableConverters;
import util.LS;
import util.StockItem;
import java.util.Iterator;
import java.util.Scanner;

public class Leapheng {

    Scanner sc = new Scanner(System.in);
    int id;
    boolean isFound;
    String deopt;

    SearchByName searchByName;
    public Leapheng(){
        searchByName=new SearchByName();
    }
//(Delete and Delete by Id)---------------------------------------------------------------------------------------------------------------------
    public void removeItem() {

        System.out.println("Input ID you want to delete : ");
        id = sc.nextInt();

        Iterator<StockItem> item = LS.listMain.iterator();
        while (item.hasNext()) {
            StockItem product = item.next();
            if (product.getId() == id) {
                readItem(id);
                System.out.print("Are you sure want to delete this record? [Y/y] or [N/n]");
                deopt = sc.next();
                if (deopt.equalsIgnoreCase("y")) {
                    //Add delete item to deleteList
                    LS.deleteList.add(new StockItem(product.getId(), product.getName(), product.getUnitPrice(), product.getQty(), product.getImportedDate(), product.isNew(), product.isUpdated()));
                    //Remove by id input
                    item.remove();
                    System.out.println("~~~~~~~~~~~~~~~~~~");
                    System.out.println(" Product was Remove");
                }
                if (deopt.equalsIgnoreCase("n")) {
                    System.out.println("~~~~~~~~~~~~~~~~");
                    System.out.println("   Remove Cancel");
                }
                System.out.println("~~~~~~~~~~~~~~~~~~");
            }
        }
    }

    public void removeByID(int id) {

        Iterator<StockItem> item = LS.listMain.iterator();
        while (item.hasNext()) {
            StockItem product = item.next();
            if (product.getId() == id) {
                readItem(id);
                System.out.print("Are you sure want to delete this record? [Y/y] or [N/n]");
                deopt = sc.next();
                if (deopt.equalsIgnoreCase("y")) {
                    //Add delete item to deleteList
                    LS.deleteList.add(new StockItem(product.getId(), product.getName(), product.getUnitPrice(), product.getQty(), product.getImportedDate(), product.isNew(), product.isUpdated()));
                    //Remove by id input
                    item.remove();
                    System.out.println("~~~~~~~~~~~~~~~~~~");
                    System.out.println(" Product was Remove");
                }
                if (deopt.equalsIgnoreCase("n")) {
                    System.out.println("~~~~~~~~~~~~~~~~");
                    System.out.println("   Remove Cancel");
                }
                System.out.println("~~~~~~~~~~~~~~~~~~");
            }
        }
    }

    public void readItem(int id) {
        for (int i = 0; i < LS.listMain.size(); i++) {
            if (LS.listMain.get(i) != null) {
                if (id == LS.listMain.get(i).getId()) {
                    System.out.println("====================================");
                    System.out.println("Product's ID: " + LS.listMain.get(i).getId());
                    System.out.println("Product's Name: " + LS.listMain.get(i).getName());
                    System.out.println("Product's Price: " + LS.listMain.get(i).getUnitPrice());
                    System.out.println("Product's Qty: " + LS.listMain.get(i).getQty());
                    System.out.println("Imported Date: " + LS.listMain.get(i).getImportedDate());
                    System.out.println("====================================");
                    isFound = true;
                }
            }
        }
        if (!isFound) {
            System.out.println(id + " Not Found!");
        }
    }

    // Search ---------------------------------------------------------------------------------------------------------------
    public void SerchName(){
        searchByName.search();
    }
    public void help(){
            System.out.println("============================================ (HELP) ==========================================");
            System.out.println("!   1.  Press   *  : Display all record of products                                          !");
            System.out.println("!   2.  Press   w  : Add new product                                                         !");
            System.out.println("!       Press   w#proname-unitprice-qty : sortcut for add new product                        !");
            System.out.println("!   3   Press   r  : read Content any content                                                !");
            System.out.println("!       Press   wr#proID  : sortcut for read prduct by Id                                    !");
            System.out.println("!   4.  Press   u  : Update Data                                                             !");
            System.out.println("!   5.  Press   d  : Delete Data                                                             !");
            System.out.println("!       Press   d#proID   : sortcut for delete prduct by Id                                  !");
            System.out.println("!   6.  Press   f  : Display first page                                                      !");
            System.out.println("!   7.  Press   p  : Display Previous page                                                   !");
            System.out.println("!   8.  Press   n  : Display Next page                                                       !");
            System.out.println("!   9.  Press   l  : Display last page                                                       !");
            System.out.println("!   10  Press   s  : Search product by name                                                  !");
            System.out.println("!   11. Press   sa : Save record to file                                                     !");
            System.out.println("!   12. Press   ba : Backup data                                                             !");
            System.out.println("!   13. Press   re : Restore data                                                            !");
            System.out.println("!   14. Press   h  : Help                                                                    !");
            System.out.println("╚════════════════════════════════════════════════════════════════════════════════════════════╝");
    }
}

