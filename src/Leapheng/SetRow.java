package Leapheng;

import com.jakewharton.fliptables.FlipTableConverters;
import util.LS;

import java.util.Scanner;

public class SetRow {

    static void ShowTable() {
        int allPage;
        int currentPage = 1;
        int row;

        Scanner sc= new Scanner(System.in);

        System.out.println("Set row : ");
        row=sc.nextInt();

        Object[][] data = new Object[row][5];
        String[] headers = {"ID       ", "PRODUCT NAME          ", "UNIT_PRICE          ", "QUANTITY  ", "IMPORTED_DATE    "};
        allPage = (int) Math.ceil((float) LS.listMain.size() / row);

        if (LS.listMain.get(0) == null) {
            System.out.println("============= PRODUCT INFORMATION =============");
            System.out.println("\n >>>>>>>   No Product To Display  <<<<<<<");
        } else {
            for (int i = 0; i < 5; i++) {
                try {
                    data[i][0] = LS.listMain.get(i).getId();
                    data[i][1] = LS.listMain.get(i).getName();
                    data[i][2] = LS.listMain.get(i).getUnitPrice();
                    data[i][3] = LS.listMain.get(i).getQty();
                    data[i][4] = LS.listMain.get(i).getImportedDate();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            System.out.println("=================================== ALL PRODUCTS INFORMATION =================================");
            System.out.print(FlipTableConverters.fromObjects(headers, data));
            System.out.println("║ Page: " + currentPage + " of " + allPage + "                                                                               ║");
            System.out.println("╚════════════════════════════════════════════════════════════════════════════════════════════╝");

        }
    }
}
