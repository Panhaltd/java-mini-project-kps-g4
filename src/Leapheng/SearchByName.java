package Leapheng;

import com.jakewharton.fliptables.FlipTable;
import util.LS;
import util.StockItem;

import java.util.Scanner;

public class SearchByName {


    Scanner sc;
    public void search() {
        sc = new Scanner(System.in);
        String[] header = {"ID", "Name", "Unit Price", "Quantity", "Imported"};
        String inputName;
        Boolean isContain = false;
        int row = LS.listMain.size();
        int i = 0, index = 0;

        System.out.print("Search by name : ");
        inputName = sc.next();
        for (StockItem ite : LS.listMain) {
            String s1 = ite.getName().toLowerCase();
            String s2 = inputName.toLowerCase();
            if (s1.contains(s2)) {
                index++;
            }
        }
        String[][] item = new String[index][5];
        for (StockItem ite : LS.listMain) {
            String s1 = ite.getName().toLowerCase();
            String s2 = inputName.toLowerCase();
            if (s1.contains(s2)) {

                item[i][0] = String.valueOf(ite.getId());
                item[i][1] = ite.getName();
                item[i][2] = String.valueOf(ite.getUnitPrice());
                item[i][3] = String.valueOf(ite.getQty());
                item[i][4] = ite.getImportedDate();
                i++;
                isContain = true;

            }
        }
        if (!isContain) {
            System.out.println("Cannot fine Item's name similar : " + inputName + " !!");
        }
        System.out.println(FlipTable.of(header, item));
    }
}