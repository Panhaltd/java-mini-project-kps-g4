package Leapheng;

import util.LS;
import util.StockItem;
import java.util.Iterator;
import java.util.Scanner;

public class Delete {
    Scanner sc = new Scanner(System.in);

    int id;
    boolean isFound;
    String deopt;

    public void removeItem() {

        System.out.println("Input ID you want to delete : ");
        id = sc.nextInt();

        Iterator<StockItem> item = LS.listMain.iterator();
        while (item.hasNext()) {
            StockItem product = item.next();
            if (product.getId() == id) {
                readItem(id);
                System.out.print("Are you sure want to delete this record? [Y/y] or [N/n]");
                deopt = sc.next();
                if (deopt.equalsIgnoreCase("y")) {
                    //Add delete item to deleteList
                    LS.deleteList.add(new StockItem(product.getId(), product.getName(), product.getUnitPrice(), product.getQty(), product.getImportedDate(), product.isNew(), product.isUpdated()));
                    //Remove by id input
                    item.remove();
                    System.out.println("~~~~~~~~~~~~~~~~~~");
                    System.out.println(" Product was Remove");
                }
                if (deopt.equalsIgnoreCase("n")) {
                    System.out.println("~~~~~~~~~~~~~~~~");
                    System.out.println("   Remove Cancel");
                }
                System.out.println("~~~~~~~~~~~~~~~~~~");
            }
        }
    }
    public void removeByID(int id) {

        Iterator<StockItem> item = LS.listMain.iterator();
        while (item.hasNext()) {
            StockItem product = item.next();
            if (product.getId() == id) {
                readItem(id);
                System.out.print("Are you sure want to delete this record? [Y/y] or [N/n]");
                deopt = sc.next();
                if (deopt.equalsIgnoreCase("y")) {
                    //Add delete item to deleteList
                    LS.deleteList.add(new StockItem(product.getId(), product.getName(), product.getUnitPrice(), product.getQty(), product.getImportedDate(), product.isNew(), product.isUpdated()));
                    //Remove by id input
                    item.remove();
                    System.out.println("~~~~~~~~~~~~~~~~~~");
                    System.out.println(" Product was Remove");
                }
                if (deopt.equalsIgnoreCase("n")) {
                    System.out.println("~~~~~~~~~~~~~~~~");
                    System.out.println("   Remove Cancel");
                }
                System.out.println("~~~~~~~~~~~~~~~~~~");
            }
        }
    }

    public void readItem(int id) {
        for (int i = 0; i < LS.listMain.size(); i++) {
            if (LS.listMain.get(i) != null) {
                if (id == LS.listMain.get(i).getId()) {
                    System.out.println("====================================");
                    System.out.println("Product's ID: " + LS.listMain.get(i).getId());
                    System.out.println("Product's Name: " + LS.listMain.get(i).getName());
                    System.out.println("Product's Price: " + LS.listMain.get(i).getUnitPrice());
                    System.out.println("Product's Qty: " + LS.listMain.get(i).getQty());
                    System.out.println("Imported Date: " + LS.listMain.get(i).getImportedDate());
                    System.out.println("====================================");
                    isFound = true;
                }
            }
        }
        if (!isFound) {
            System.out.println(id + " Not Found!");
        }
    }
}