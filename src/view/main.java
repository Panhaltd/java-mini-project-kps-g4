package view;
import Leapheng.Leapheng;
import Rofak.DBUtils;
import Rofak.WriteRead;
import constant.ConstStr;
import model.FunctionStorage;
import model.Kheatbung;
import model.Panha;
import util.LS;
import util.StockItem;

import java.time.LocalDate;
import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        String opt;
        Scanner sc = new Scanner(System.in);
        FunctionStorage fs = new FunctionStorage();
        Kheatbung kheatbung = new Kheatbung();
        Panha panha = new Panha();
        Leapheng leapheng = new Leapheng();
        fs.printGroupName();
        WriteRead wr = new WriteRead();
        fs.getDataFromDatabase();
        LS.allpage = (int) Math.ceil((float)LS.listMain.size()/LS.srow);
        int id=0;
        while (true) {
            fs.printMenu();
            System.out.print("Command --> ");
            opt = sc.next();
            if(opt.length()<3){
                switch (opt) {
                    case "*":
                        panha.display();
                        break;
                    case "w":
                        wr.write();
                        break;
                    case "r":
                        id= DBUtils.isNumber("Enter Product's ID:");
                        wr.readByID(id);
                        break;
                    case "u":
                        panha.update();
                        break;
                    case "d":
                        leapheng.removeItem();
                        break;
                    case "f":
                        kheatbung.first();
                        break;
                    case "p":
                        kheatbung.previous();
                        break;
                    case "n":
                        kheatbung.next();
                        break;
                    case "l":
                        kheatbung.last();
                        break;
                    case "s":
                        leapheng.SerchName();
                        break;
                    case "g":
                        kheatbung.gotoPage();
                        break;
                    case "se":
                        kheatbung.setRow();
                        break;
                    case "sa":
                        wr.saveAllData("stockdata");
                        break;
                    case "ba":
                        panha.backUp();
                        break;
                    case "re":
                        panha.restore();
                        break;
                    case "h":
                        leapheng.help();
                        break;
                    case "e":
                        System.out.println("I Love you 3000!");
                        System.out.println("Good Bye!! :') ");
                        System.exit(0);
                        break;
                    default:
                        System.out.println(ConstStr.INVALID);
                        break;
                }
            }else if (opt.equalsIgnoreCase("_10m")){
                panha.create10mRecord();
            }
            else{
                String[] cmd = opt.split("#");
                // Shortcut for write
                try {

                if (cmd[0].equalsIgnoreCase("w")) {
                    String[] value = cmd[1].split("-");
                    String name, unitPrice, qty;
                    LocalDate importedDate;
                    name = value[0];
                    unitPrice = value[1];
                    qty = value[2];
                    importedDate = LocalDate.now();
                    if (LS.listMain.isEmpty()) {
                        id = DBUtils.getLastRecord("stockdata");
                    } else {

                        id = DBUtils.getMax(LS.listMain) + 1;
                    }
                    System.out.print("Are you sure you want to add this record [Y/y] or [N/n]:");
                    char save = sc.next().charAt(0);
                    if (save == 'y' || save == 'Y') {
                        LS.listMain.add(new StockItem(id, name, Double.valueOf(unitPrice), Integer.valueOf(qty), importedDate.toString(), true, false));
                        wr.addRecovery("\"recovery\"", name, Double.valueOf(unitPrice), Integer.valueOf(qty), importedDate.toString());
                        //Recovery(list);
                    }
                    //Shortcut for read
                } else if (cmd[0].equalsIgnoreCase("r")) {
                    id = Integer.valueOf(cmd[1]);
                    wr.readByID(id);
                    //Shortcut for Delete
                } else if (cmd[0].equalsIgnoreCase("d")) {
                    //code
                    id = Integer.valueOf(cmd[1]);
                    leapheng.removeByID(id);
                }else {
                    System.out.println(ConstStr.INVALID);
                }
            }catch (Exception e){
                    System.out.println(e.getMessage());
                }
            }
        }

    }

}
