package util;

public class StockItem{
    private long id10m;
    private int id;
    private String Name;
    private Double unitPrice;
    private int qty;
    private String importedDate;
    private boolean isNew;
    private boolean isUpdated;

    public StockItem(int id, String name, Double unitPrice, int qty, String importedDate, Boolean isNew, Boolean isUpdated) {
        this.id = id;
        Name = name;
        this.unitPrice = unitPrice;
        this.qty = qty;
        this.importedDate = importedDate;
        this.isNew = isNew;
        this.isUpdated = isUpdated;
    }

    public StockItem(long id10m, String name, Double unitPrice, int qty, String importedDate, Boolean isNew, Boolean isUpdated) {
        this.id10m = id10m;
        Name = name;
        this.unitPrice = unitPrice;
        this.qty = qty;
        this.importedDate = importedDate;
        this.isNew = isNew;
        this.isUpdated = isUpdated;
    }

    public StockItem(String name, Double unitPrice, int qty, String importedDate, Boolean isNew, Boolean isUpdated) {
        Name = name;
        this.unitPrice = unitPrice;
        this.qty = qty;
        this.importedDate = importedDate;
        this.isNew = isNew;
        this.isUpdated = isUpdated;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getImportedDate() {
        return importedDate;
    }

    public void setImportedDate(String importedDate) {
        this.importedDate = importedDate;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public boolean isUpdated() {
        return isUpdated;
    }

    public void setUpdated(boolean updated) {
        isUpdated = updated;
    }

}
