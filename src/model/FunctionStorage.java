package model;

import com.jakewharton.fliptables.FlipTable;
import constant.ConstStr;
import util.LS;
import util.StockItem;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FunctionStorage extends Thread{
    public void getDataFromDatabase(){
        Connection con;
        Statement presmt;
        try {
            long start = System.currentTimeMillis();
            Class.forName("org.postgresql.Driver");
            con = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/kps-g4"
                            , "postgres", "Panhalong11");
            presmt = con.createStatement();
            ResultSet rs = presmt.executeQuery("SELECT * FROM stockdata");
            while (rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                Double unitPrice = rs.getDouble("unitPrice");
                int qty = rs.getInt("qty");
                String data = rs.getString("importedDate");
                LS.listMain.add(new StockItem(id,name,unitPrice,qty,data,false,false));
            }
            LS.listMain.sort(Comparator.comparingLong(StockItem::getId));
            con.close();
            rs.close();
            long end = System.currentTimeMillis();
            long loading = end - start;
            System.out.println("loading--> "+loading+"ms");
        }catch (Exception e){
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    public void printGroupName(){
        System.out.println("\t\t\t\t\t\t\t\t\t\tWelcome To");
        System.out.println("\t\t\t\t\t\t\t\t\tStock Management");
        try {
            String[] str = ConstStr.groupName.split("\n");
            for (String s : str) {
                System.out.println(s);
                sleep(300);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void printMenu(){
        String[] menu1 = {"*)Display","W)rite","R)ead","U)pdate","D)elete","F)irst","P)revios","N)ext","L)ast"};
        String[][] menu2 = {
                { "S)earch","G)oto","Se)t row","Sa)ve","Ba)ck up","Re)store","H)elp","E)xit"," "},
        };
        System.out.println(FlipTable.of(menu1,menu2));
    }

    public boolean isNumber(String str){
        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }
}
