package model;

import util.LS;

import javax.swing.plaf.IconUIResource;
import java.util.Scanner;

public class Kheatbung {
    FunctionStorage fs = new FunctionStorage();
    Panha panha = new Panha();
    Scanner sc = new Scanner(System.in);
    public void next(){
        if (LS.currentPage==LS.allpage) LS.currentPage=1;
        else LS.currentPage++;
        panha.display();
    }
    public void previous(){
        if (LS.currentPage==1) LS.currentPage=LS.allpage;
        else LS.currentPage--;
        panha.display();
    }
    public void last(){
        LS.currentPage=LS.allpage;
        panha.display();
    }
    public void first(){
        LS.currentPage=1;
        panha.display();
    }
    public void gotoPage(){
        boolean isTrue=true;
        while (isTrue) {
            System.out.print("Enter Number of Page : ");
            String gotoPage = sc.next();
            if (fs.isNumber(gotoPage)) {
                if (Integer.parseInt(gotoPage) <= LS.allpage) {
                    LS.currentPage = Integer.parseInt(gotoPage);
                    panha.display();
                    isTrue = false;
                } else {
                    System.out.println("Please enter number from 1 to " + LS.allpage);
                }
            }
        }
    }
    public void setRow(){
        boolean isTrue=true;
        while (isTrue){
        System.out.print("Enter new amount of row : ");
        String nrow= sc.next();
        if (fs.isNumber(nrow))
        LS.srow=Integer.parseInt(nrow);
        LS.currentPage=1;
        LS.allpage = (int) Math.ceil((float)LS.listMain.size()/LS.srow);
        panha.display();
        }
    }
}
