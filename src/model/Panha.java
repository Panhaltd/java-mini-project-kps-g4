package model;

import com.jakewharton.fliptables.FlipTable;
import com.jakewharton.fliptables.FlipTableConverters;
import constant.ConstStr;
import org.apache.log4j.varia.NullAppender;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;
import util.LS;
import util.StockItem;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class Panha {

    static Scanner sc = new Scanner(System.in);
    FunctionStorage fs = new FunctionStorage();

    public void display() {
        int j=0;
        String[] header = {"ID", "Name", "Unit Price", "Quantity", "Imported Date"};
        try {
            int i=0;
            int firstrecord,lastrecord;
            firstrecord=LS.currentPage*LS.srow-LS.srow;
            lastrecord=LS.currentPage*LS.srow;
            if (LS.listMain.size()==0) {
                System.out.println("============= PRODUCT INFORMATION =============");
                System.out.println("»»»>         No Product To Display         «««<");
                System.out.println("===============================================");
            }
            else {
                int exactRow=LS.srow;
                if (LS.currentPage==LS.allpage){
                    exactRow=LS.listMain.size()-((LS.currentPage-1)*LS.srow);
                }
                String[][] item = new String[exactRow][5];
                if (exactRow<LS.srow) {
                    lastrecord=firstrecord+exactRow;
                }
                System.out.println(lastrecord);
                for (i = firstrecord; i < lastrecord; i++){
                    try {
                        item[j][0] = Integer.toString(LS.listMain.get(i).getId());
                        item[j][1] = LS.listMain.get(i).getName();
                        item[j][2] = Double.toString(LS.listMain.get(i).getUnitPrice());
                        item[j][3] = Integer.toString(LS.listMain.get(i).getQty());
                        item[j][4] = LS.listMain.get(i).getImportedDate();
                        j++;
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }
                System.out.println("=================================== ALL PRODUCTS INFORMATION =================================");
                System.out.print(FlipTableConverters.fromObjects(header, item));
                org.apache.log4j.BasicConfigurator.configure(new NullAppender());
                Table table = new Table(4, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.SURROUND);
                table.addCell(" Page :");
                table.addCell(""+LS.currentPage+"/"+LS.allpage);
                table.addCell(" ║ Total Records : ");
                table.addCell(""+LS.listMain.size());
                System.out.println(table.render());
            }
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + " : " + e.getMessage());
        }
    }

    public void update(){
        String nName,nImportedDate;
        Double nUnitPrice;
        int nQty;
        Boolean isChecked=true,found=false,isTrue=true;
        while (isTrue){
            System.out.print("Enter ID To Update: ");
            String uId = sc.next();
            if (fs.isNumber(uId)){
                isTrue=false;
                for(StockItem item:LS.listMain){
                    if (item.getId()==Integer.parseInt(uId)){
                        System.out.println("ID : "+item.getId());
                        System.out.println("Name : "+item.getName());
                        System.out.println("Unit Price : "+item.getUnitPrice());
                        System.out.println("Quantity : "+item.getQty());
                        System.out.println("Imported Date : "+item.getImportedDate());
                        System.out.println();
                        System.out.println("====================================== Set New Data ======================================");
                        System.out.println("||  1). Name    2).Unit Price   3). Quantity    4). Imported Date   5). Back To Menu    ||");
                        System.out.println("==========================================================================================");
                        while (isChecked){
                            System.out.print("Enter An Option : ");
                            int opt = sc.nextInt();
                            switch (opt) {
                                case 1:
                                    isChecked = false;
                                    System.out.print("Enter Product New Name : ");
                                    sc.nextLine();
                                    nName = sc.nextLine();
                                    LS.listMain.set(item.getId() - 1, new StockItem(item.getId(), nName, item.getUnitPrice(), item.getQty(), item.getImportedDate(), item.isNew(), true));
                                    break;
                                case 2:
                                    isChecked = false;
                                    System.out.print("Enter Product New Unit Price : ");
                                    nUnitPrice = sc.nextDouble();
                                    LS.listMain.set(item.getId() - 1, new StockItem(item.getId(), item.getName(), nUnitPrice, item.getQty(), item.getImportedDate(), item.isNew(), true));
                                    break;
                                case 3:
                                    isChecked = false;
                                    System.out.print("Enter Product New Quantity : ");
                                    nQty = sc.nextInt();
                                    LS.listMain.set(item.getId() - 1, new StockItem(item.getId(), item.getName(), item.getUnitPrice(), nQty, item.getImportedDate(), item.isNew(), true));
                                    break;
                                case 4:
                                    isChecked = false;
                                    System.out.print("Enter Product New Imported Date : ");
                                    sc.nextLine();
                                    nImportedDate = sc.nextLine();
                                    LS.listMain.set(item.getId() - 1, new StockItem(item.getId(), item.getName(), item.getUnitPrice(), item.getQty(), nImportedDate, item.isNew(), true));
                                    break;
                                case 5:
                                    isChecked = false;
                                    break;
                                default:
                                    System.out.println(ConstStr.INVALID);
                            }
                        }
                        found=true;
                    }
                }
                if (!found){
                    System.out.println("Stock doesn't have this ID!");
                }
            }
        }
    }

    public void backUp() {
        System.out.println("Backup to An Exits File or Create New File? ");
        System.out.println("1). An Exits File       2). A New FIle      3). Back To Menu");
        System.out.print("Enter -> ");
        String opt = sc.next();
        switch (opt) {
            case "1":
                exitsFile();
                break;
            case "2":
                newFile();
                break;
            default:
                System.out.println(ConstStr.INVALID);
                break;
        }
    }

    public void exitsFile() {
        int count = 1;
        ArrayList<String> fileList = new ArrayList<>();
        try {
            String[] pathnames;
            File f = new File("../KPS-G4-JAVA-MINI-PROJECT");
            pathnames = f.list();

            for (String pathname : pathnames) {
                if (pathname.endsWith(".txt")) {
                    fileList.add(pathname);
                    System.out.println(count + ")." + pathname);
                    count++;
                }
            }
            System.out.println("Enter File Number To Back Up : ");
            int fileNum = sc.nextInt();
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileList.get(fileNum - 1), true));
            for (StockItem item : LS.listMain) {
                bufferedWriter.write(item.getId() + "," + item.getName() + "," + item.getUnitPrice() + "," +
                        "" + item.getQty() + "," + item.getImportedDate() + "," + item.isNew() + "," + item.isUpdated());
                bufferedWriter.newLine();
            }
            System.out.println("Back Up To File : "+fileList.get(fileNum-1));
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void newFile() {
        System.out.print("Enter New File Name : ");
        String nFile = sc.next();
        File file = new File(nFile);
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, true));
            for (StockItem item : LS.listMain) {
                bufferedWriter.write(item.getId() + "," + item.getName() + "," + item.getUnitPrice() + "," +
                        "" + item.getQty() + "," + item.getImportedDate() + "," + item.isNew() + "," + item.isUpdated());
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
            System.out.println("Back Up To File Named : " + nFile + " Successfully!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void restore() {
        int count = 1;
        boolean isNew=true;
        Connection con;
        Statement smt,stm2;
        ArrayList<String> fileList = new ArrayList<>();
        try {
            Class.forName("org.postgresql.Driver");
            con = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/kps-g4"
                            , "postgres", "Panhalong11");
            String[] pathnames;
            File f = new File("../KPS-G4-JAVA-MINI-PROJECT");
            pathnames = f.list();

            for (String pathname : pathnames) {
                if (pathname.endsWith(".txt")) {
                    fileList.add(pathname);
                    System.out.println(count + ")." + pathname);
                    count++;
                }
            }

            System.out.print("Enter File Number To Restore : ");
            int fileNum = sc.nextInt();
            System.out.println("1). Append Data    2). Replace Data");
            System.out.print("Enter an Option : ");
            int opt=sc.nextInt();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileList.get(fileNum-1)) {
            });
            String str;
                if (opt==1){
                    long start = System.currentTimeMillis();
                    while ((str = bufferedReader.readLine()) != null) {
                        String field[] = str.split(",");
                        for (StockItem item : LS.listMain) {
                            if (Integer.parseInt(field[0]) == item.getId()) {
                                isNew = false;
                            }
                        }
                        if (isNew) {
                            LS.listMain.add(new StockItem(Integer.parseInt(field[0]), field[1],
                                    Double.parseDouble(field[2]), Integer.parseInt(field[3]),
                                    field[4], true, Boolean.parseBoolean(field[6])));
                            smt = con.createStatement();
                            smt.executeUpdate("INSERT INTO recovery (name,unitPrice,qty,importedDate) VALUES ('" + field[1] + "'," + Double.parseDouble(field[2]) + "," + Integer.parseInt(field[3]) + ",'" + field[4] + "')");
                            System.out.println("Restore record named : " + field[1] + " successfully!");
                        }
                        isNew = true;
                    }
                    LS.allpage = (int) Math.ceil((float)LS.listMain.size()/LS.srow);
                    LS.listMain.sort(Comparator.comparingLong(StockItem::getId));
                    long end = System.currentTimeMillis();
                    long loading = end - start;
                    System.out.println("loading--> "+loading+"ms");
                }else if (opt==2) {
                    long start = System.currentTimeMillis();
                    LS.listMain.clear();
                    stm2 = con.createStatement();
                    stm2.executeUpdate("DELETE FROM stockdata");
                    while ((str = bufferedReader.readLine()) != null) {
                        String field[] = str.split(",");
                            LS.listMain.add(new StockItem(Integer.parseInt(field[0]), field[1],
                                    Double.parseDouble(field[2]), Integer.parseInt(field[3]),
                                    field[4], true, Boolean.parseBoolean(field[6])));

                            smt = con.createStatement();
                            smt.executeUpdate("INSERT INTO recovery (name,unitPrice,qty,importedDate) VALUES ('" + field[1] + "'," + Double.parseDouble(field[2]) + "," + Integer.parseInt(field[3]) + ",'" + field[4] + "')");
                            System.out.println("Restore record named : " + field[1] + " successfully!");

                    }
                    LS.allpage = (int) Math.ceil((float)LS.listMain.size()/LS.srow);
                    LS.listMain.sort(Comparator.comparingLong(StockItem::getId));
                    long end = System.currentTimeMillis();
                    long loading = end - start;
                    System.out.println("loading--> "+loading+"ms");
                }else {
                    System.out.println(ConstStr.INVALID);
                }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void create10mRecord(){
        Connection con;
        Statement stm,stm2;
        try {
            long start = System.currentTimeMillis();
            Class.forName("org.postgresql.Driver");
            con = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/kps-g4"
                            , "postgres", "Panhalong11");
            stm = con.createStatement();
            stm.executeUpdate("DELETE FROM stockdata");
            stm.close();
            LS.listMain.clear();
            int k=1;
            for (long i=0;i<10000000;i++){
                LS.listMain.add(new StockItem(k,"10m Record",10.10,10,"10-10-2010",true,false));
                k++;
            }
            LS.allpage = (int) Math.ceil((float)LS.listMain.size()/LS.srow);
            long end = System.currentTimeMillis();
            long loading = end - start;
            System.out.println("loading--> "+loading+"ms");
            System.out.println("Create 10 million records successfully!");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
