package Rofak;
import util.StockItem;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DBUtils {
    public static Connection connectDB(String dbName,String userName,String pass) {
        Connection c = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/"+dbName,
                                        userName, pass);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return c;
    }

    public static void closeConnection(ResultSet rs, PreparedStatement preparedStatement, Connection c){
        try {
            rs.close();
            preparedStatement.close();
            c.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
    public static int getLastRecord(String tb){
        Connection con=connectDB("Test","postgres","Jupiter");
        Statement st=null;
        ResultSet rs=null;
        int id=0;
        try {
            st=con.createStatement();
//            rs=st.executeQuery("SELECT * FROM "+tb+" ORDER BY Id DESC LIMIT 1");
            rs=st.executeQuery("SELECT MAX(id) AS LargestPrice FROM stockdata; ");
            while (rs.next()){
                id=rs.getInt("LargestPrice");
            }
            id++;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return id;
    }
    public static boolean isNumeric(String str) {
        return str != null && str.matches("[-+]?\\d*\\.?\\d+");
    }
    public static int isNumber(String str){
        Scanner in=new Scanner(System.in).useDelimiter("\n");
        boolean isNotNumber=true;
        int value=0;
        while (isNotNumber){
            System.out.print(str);
            String myValue=in.next();
            if (isNumeric(myValue)){
                value=Integer.parseInt(myValue);
                isNotNumber=false;
            }else {
                System.out.println("Please Input Number!");
                isNotNumber=true;
            }
        }
        return value;
    }
    public static Double isDouble(String str){
        Scanner in=new Scanner(System.in).useDelimiter("\n");
        boolean isNotNumber=true;
        double value=0;
        while (isNotNumber){
            System.out.print(str);
            String myValue=in.next();
            if (isNumeric(myValue)){
                value=Double.parseDouble(myValue);
                isNotNumber=false;
            }else {
                System.out.println("Please Input Number!");
                isNotNumber=true;
            }
        }
        return value;
    }

public static int getMax(List<StockItem> list){
    int max = Integer.MIN_VALUE;
    for(int i=0; i<list.size(); i++){
        if(list.get(i).getId() > max){
            max = list.get(i).getId();
        }
    }
    return max;
}
}
