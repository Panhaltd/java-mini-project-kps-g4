package Rofak;

import java.time.LocalDate;
public class Products {
    private int id;
    private String name;
    private float price;
    private int qty;
    private LocalDate date;
    private boolean isNow=false;
    private boolean isUpate=false;

    public Products(int id,String name, float price, int qty, LocalDate date,boolean isNow ,boolean isUpate) {
        this.id=id;
        this.name = name;
        this.price = price;
        this.qty = qty;
        this.date = date;
        this.isNow=isNow;
        this.isUpate=isUpate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isNow() {
        return isNow;
    }

    public void setNow(boolean now) {
        isNow = now;
    }

    public boolean isUpate() {
        return isUpate;
    }

    public void setUpate(boolean upate) {
        isUpate = upate;
    }
}
