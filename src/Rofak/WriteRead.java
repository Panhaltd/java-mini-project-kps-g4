package Rofak;

import org.apache.log4j.varia.NullAppender;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;
import util.LS;
import util.StockItem;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class WriteRead {
    Connection con=null;
    PreparedStatement ps=null;
    Statement st=null;
    ResultSet rs=null;
    public static int id=0;
    private Scanner in;
    private String name;
    private double price;
    private int qty;
    private String date;
//    private List<Products> list=new ArrayList<>();
    private String saveToList;
    private char save;
    private boolean isSave=false;
    // write to database
    public WriteRead(){

        List<Products> recoveryList=new ArrayList <>();
        in=new Scanner(System.in).useDelimiter("\n");
        con=DBUtils.connectDB("kps-g4","postgres","Panhalong11");
        try {
            st=con.createStatement();
            rs=st.executeQuery("SELECT * FROM \"recovery\"");
            //if have recovery add to recoveryList (get from Database)
            while (rs.next()){
                recoveryList.add(new Products(rs.getInt("id"),rs.getString("name"),rs.getFloat("unitprice"),rs.getInt("qty"),rs.getDate("importeddate").toLocalDate(),false,false));
            }
            //check if recoveryList is not empty
            if(!recoveryList.isEmpty()){
                   for(int i=0;i<recoveryList.size();i++){
                       Products products=recoveryList.get(i);
                       System.out.print("Do you want to save "+products.getName()+" ! [Y/n] or [N/n]:");
                       save=in.next().charAt(0);
                       if(save=='Y' || save=='y') {
                           if (saveToDB("stockdata", products.getName(), products.getPrice(), products.getQty(), products.getDate())) {
                               isSave = true;
                           }
                       }else {
                        // delete data in Recovery after click no
                        deleteAll("\"recovery\"",con);
                        }

                   }


            }
            if(isSave==true){
                // delete data in Recovery after click no
                System.out.println("Saved Successfully!");
                deleteAll("\"recovery\"",con);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public void write(){
        con=DBUtils.connectDB("kps-g4","postgres","Panhalong11");
        if(LS.listMain.isEmpty()){
            id=DBUtils.getLastRecord("stockdata");
        }else {

            id=DBUtils.getMax(LS.listMain)+1;
        }

        System.out.println("Product's ID: " + id);
        System.out.print("Product's Name: ");
        name = in.next();
        price =DBUtils.isDouble("Product's Price: ");
        qty =DBUtils.isNumber("Product's Qty: ");
        date = LocalDate.now().toString();
        //show data after in input
        org.apache.log4j.BasicConfigurator.configure(new NullAppender());
        Table table = new Table(3, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.SURROUND);
        table.addCell("Product's ID: ");
        table.addCell("\t : ");
        table.addCell(""+id);
        table.addCell("Product's Name: ");
        table.addCell("\t : ");
        table.addCell(name);
        table.addCell("Product's Price: ");
        table.addCell("\t : ");
        table.addCell(""+price);
        table.addCell("Product's Qty: ");
        table.addCell("\t : ");
        table.addCell(""+qty);
        table.addCell("Imported Date:");
        table.addCell("\t : ");
        table.addCell(""+date);
        System.out.println(table.render());
        System.out.print("Are you sure you want to add this record [Y/y] or [N/n]:");
        saveToList = in.next();
        if (saveToList.equalsIgnoreCase("y")) {
            LS.listMain.add(new StockItem(id,name, price, qty, date,true,false));
            addRecovery("\"recovery\"",name,price,qty,date);
            //Recovery(list);
        }
    }
    public void readByID(int id){
        boolean isfound=false;
        for(int i=0;i<LS.listMain.size();i++){
            if(LS.listMain.get(i)!=null){
                if(id==LS.listMain.get(i).getId()){
                    org.apache.log4j.BasicConfigurator.configure(new NullAppender());
                    Table table = new Table(3, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.SURROUND);
                    table.addCell("Product's ID: ");
                    table.addCell("\t : ");
                    table.addCell(""+LS.listMain.get(i).getId());
                    table.addCell("Product's Name: ");
                    table.addCell("\t : ");
                    table.addCell(LS.listMain.get(i).getName());
                    table.addCell("Product's Price: ");
                    table.addCell("\t : ");
                    table.addCell(""+LS.listMain.get(i).getUnitPrice());
                    table.addCell("Product's Qty: ");
                    table.addCell("\t : ");
                    table.addCell(""+LS.listMain.get(i).getQty());
                    table.addCell("Imported Date:");
                    table.addCell("\t : ");
                    table.addCell(""+LS.listMain.get(i).getImportedDate());
                    System.out.println(table.render());
                    isfound=true;
                }
            }
        }
        if(isfound!=true){
            System.out.println("ID : "+id+" Not Found!");
        }
    }
    public boolean saveToDB(String tb,String name,float price,int qty,LocalDate date){
        boolean isSave=false;
//        con=DBUtils.connectDB("kps-g4","postgres","rofak@123");
        try {
            ps = con.prepareStatement("INSERT INTO "+tb+" (name,\"unitprice\", qty,\"importeddate\") VALUES (?,?,?,?)");
            ps.setString(1,name);
            ps.setFloat(2,price);
            ps.setInt(3,qty);
            ps.setDate(4, Date.valueOf(date));
            if (ps.executeUpdate() >= 1) {
               isSave=true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isSave;
    }
    public void addRecovery(String tb,String name,double price, int qty,String date ){
//        con=DBUtils.connectDB("kps-g4","postgres","rofak@123");
        try {
            ps = con.prepareStatement("INSERT INTO "+tb+" (name,\"unitprice\", qty,\"importeddate\") VALUES (?,?,?,?)");
            ps.setString(1,name);
            ps.setDouble(2,price);
            ps.setInt(3,qty);
            ps.setDate(4, Date.valueOf(date));
            if (ps.executeUpdate() >= 1) {
                System.out.println("Saved Successfully!");
            }
//            con.close();
//            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void deleteAll(String tableName,Connection connection){
        try {
            st=connection.createStatement();
            st.executeUpdate("DELETE FROM "+ tableName);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public ResultSet selectAll(Connection connection,String tableName){
        Statement statement;
        ResultSet resultSet=null;
        try {
            statement=connection.createStatement();
            resultSet=statement.executeQuery("SELECT * FROM "+tableName);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }
    public void saveAllData(String tb){
        boolean isSave=false;
        boolean isUpdated=false;
        for (int i = 0; i < LS.listMain.size(); i++) {
            if(LS.listMain.get(i)!=null){
                if(LS.listMain.get(i).isNew()==true || LS.listMain.get(i).isNew()==true ){

                    try {
                        ps = con.prepareStatement("INSERT INTO "+tb+" (name,\"unitprice\", qty,\"importeddate\") VALUES (?,?,?,?)");
                        ps.setString(1,LS.listMain.get(i).getName());
                        ps.setDouble(2,LS.listMain.get(i).getUnitPrice());
                        ps.setInt(3,LS.listMain.get(i).getQty());
                        ps.setDate(4, Date.valueOf(LS.listMain.get(i).getImportedDate()));
                        if (ps.executeUpdate() >= 1) {
                            isSave=true;
                            System.out.println("==============================");
                            System.out.println(LS.listMain.get(i).getName()+" has Saved Successfully!");
                            System.out.println("==============================");
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                else if(LS.listMain.get(i).isUpdated()==true && LS.listMain.get(i).isNew()==false){
                    try {
                        ps = con.prepareStatement("UPDATE "+tb+" SET name=?, unitprice=?, qty=?, importeddate= ? WHERE id=?");
                        ps.setString(1,LS.listMain.get(i).getName());
                        ps.setDouble(2,LS.listMain.get(i).getUnitPrice());
                        ps.setInt(3,LS.listMain.get(i).getQty());
                        ps.setDate(4, Date.valueOf(LS.listMain.get(i).getImportedDate()));
                        ps.setInt(5, LS.listMain.get(i).getId());
                        if (ps.executeUpdate() >= 1) {
                           isUpdated=true;
                            System.out.println("==============================");
                            System.out.println(LS.listMain.get(i).getName()+" Updated Successfully!");
                            System.out.println("==============================");
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        //if saved
        if(isSave==true||isUpdated==true){
            deleteAll("\"recovery\"",con);
        }
        //if update//
    }
}
